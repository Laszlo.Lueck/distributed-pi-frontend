import {ListMeasurementsService} from "./list-measurements.service";
import {HttpClient} from "@angular/common/http";


describe('ListMeasurementsService', () => {
  let service: ListMeasurementsService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    service = new ListMeasurementsService(httpClientSpy);
  });

  it('should create an instance', () => {
    expect(service).toBeTruthy();
  });
});
