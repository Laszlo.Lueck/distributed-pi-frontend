import {MeasurementByDevice} from "./measurement-by-device";

export interface ListMeasurementDistributionTimings {
  sessionId: string;
  measurements: MeasurementByDevice[];
}
