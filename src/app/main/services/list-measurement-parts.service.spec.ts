import { TestBed } from '@angular/core/testing';

import { ListMeasurementPartsService } from './list-measurement-parts.service';
import {HttpClient} from "@angular/common/http";

describe('ListMeasurementPartsService', () => {
  let service: ListMeasurementPartsService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;


  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    service = new ListMeasurementPartsService(httpClientSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
