import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ListMeasurementPartsResponse} from "./list-measurement-parts-response";
import {environment} from "../../../environments/environment";
import {catchError, map, take} from "rxjs";
import {Either} from "../../generic/Either";
import {getErrorHandler} from "../../generic/helper";

@Injectable({
  providedIn: 'root'
})
export class ListMeasurementPartsService {


  public listMeasurementParts(sessionId: string, page: number, pageSize: number) {
    return this
      .httpClient
      .get<ListMeasurementPartsResponse>(`${environment.apiUrl}calculation/parts/list/${sessionId}/${page}/${pageSize}`)
      .pipe(
        take(1),
        map(result => Either.makeRight(result)),
        catchError(getErrorHandler<ListMeasurementPartsResponse>('listMeasurementParts'))
      )
  }

  constructor(private httpClient: HttpClient) { }
}
