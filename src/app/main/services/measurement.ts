import {MeasurementDto} from "./measurementDto";

export interface Measurement{
  pageCount: number;
  docCount: number;
  measurementList: MeasurementDto[];
}
