export interface ListMeasurementPartsResponse {
  sessionId: string;
  measurementPartList: ListMeasurementPart[];
  pageCount: number;
  docCount: number;
}


export interface ListMeasurementPart {
  id: number;
  result: string;
  calculationTimeMs: number;
}
