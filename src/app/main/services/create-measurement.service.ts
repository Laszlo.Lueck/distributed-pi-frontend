import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {CreateMeasurementResponse} from "./create-measurement-response";
import {CreateMeasurementRequest} from "./create-measurement-request";
import {catchError, map, take} from "rxjs";
import {environment} from "../../../environments/environment";
import {Either} from "../../generic/Either";
import {getErrorHandler} from "../../generic/helper";

@Injectable({
  providedIn: 'root'
})
export class CreateMeasurementService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };


  public createMeasurement(iterations: number, precision: number) {
    const createMeasurementRequest: CreateMeasurementRequest = {
      iterations: iterations,
      precision: precision
    }

    return this
      .httpClient
      .put<CreateMeasurementResponse>(`${environment.apiUrl}calculation/create`, createMeasurementRequest, this.httpOptions)
      .pipe(
        take(1),
        map(result => Either.makeRight(result)),
        catchError(getErrorHandler<CreateMeasurementResponse>('createMeasurement'))
      )
  }

  constructor(private httpClient: HttpClient) {
  }
}
