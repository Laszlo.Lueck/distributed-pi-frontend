import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ListMeasurementDistributionTimings} from "./list-measurement-distribution-timings";
import {environment} from "../../../environments/environment";
import {catchError, map, Observable, take} from "rxjs";
import {Either} from "../../generic/Either";
import {getErrorHandler} from "../../generic/helper";
import {BaseError} from "../../generic/baseError";

@Injectable({
  providedIn: 'root'
})
export class MeasurementDistributionTimingService {

  public listMeasurementDistributionTimings(sessionId: string): Observable<Either<BaseError, ListMeasurementDistributionTimings>> {
    return this
      .httpClient
      .get<ListMeasurementDistributionTimings>(`${environment.apiUrl}calculation/distribution/${sessionId}`)
      .pipe(
        take(1),
        map(result => Either.makeRight(result)),
        catchError(getErrorHandler<ListMeasurementDistributionTimings>('listMeasurementDistributionTimings'))
      )
  }
  constructor(private httpClient: HttpClient) { }
}
