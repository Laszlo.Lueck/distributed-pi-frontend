import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {GetResultDto} from "./get-result-dto";
import {catchError, map, take} from "rxjs";
import {getErrorHandler} from "../../generic/helper";
import {Either} from "../../generic/Either";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class GetResultService {


  public getResultDto(sessionId: string) {
    return this
      .httpClient
      .get<GetResultDto>(`${environment.apiUrl}calculation/result/${sessionId}`)
      .pipe(
        take(1),
        map(result => Either.makeRight(result)),
        catchError(getErrorHandler<GetResultDto>('getResult'))
      )
  }


  constructor(private httpClient: HttpClient ) { }
}
