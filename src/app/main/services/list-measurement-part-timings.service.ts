import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {catchError, map, take} from "rxjs";
import {getErrorHandler} from "../../generic/helper";
import {ListMeasurementPartTimingsResponse} from "./list-measurement-part-timings-response";
import {Either} from "../../generic/Either";

@Injectable({
  providedIn: 'root'
})
export class ListMeasurementPartTimingsService {

  public listMeasurementPartTimings(sessionId: string) {
    return this
      .httpClient
      .get<ListMeasurementPartTimingsResponse>(`${environment.apiUrl}calculation/parts/timings/list/${sessionId}`)
      .pipe(
        take(1),
        map(result => Either.makeRight(result)),
        catchError(getErrorHandler<ListMeasurementPartTimingsResponse>('listMeasurementPartTimings'))
      )
  }

  constructor(private httpClient: HttpClient ) { }
}
