export interface DeleteResultDto {
  session: string;
  measurementDeleteSuccess: boolean;
  measurementPartDeleteSuccess: boolean;
  measurementPartDeletedCount: number;
}
