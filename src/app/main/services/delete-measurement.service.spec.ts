import { TestBed } from '@angular/core/testing';

import { DeleteMeasurementService } from './delete-measurement.service';
import {HttpClient} from "@angular/common/http";

describe('DeleteMeasurementService', () => {
  let service: DeleteMeasurementService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['delete']);
    service = new DeleteMeasurementService(httpClientSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
