import { FinalizeMeasurementService } from './finalize-measurement.service';
import {HttpClient} from "@angular/common/http";

describe('FinalizeMeasurementService', () => {
  let service: FinalizeMeasurementService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['put']);
    service = new FinalizeMeasurementService(httpClientSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
