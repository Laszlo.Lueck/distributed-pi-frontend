export interface CreateMeasurementResponse {
  stateText: string;
  additionalText: string;
  sessionId: string;
  state: number;
}
