export interface MeasurementDto{
  id: string;
  createDate: Date;
  iterations: number;
  precision: number;
  executing: boolean;
  checked: boolean;
  started: boolean;
  finalized: boolean;
  finalizedTimeMs: number;
  totalParts: number;
  totalFinishedParts: number;
}
