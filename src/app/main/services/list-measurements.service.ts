import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Measurement} from "./measurement";
import {catchError, map, take} from "rxjs";
import {getErrorHandler} from "../../generic/helper";
import {Either} from "../../generic/Either";
import {environment} from "../../../environments/environment";
import {SortOrder} from "../../services/sort-order";

@Injectable({
  providedIn: 'root'
})
export class ListMeasurementsService {


  public listMeasurements(sortOrder: SortOrder, page: number, pageSize: number) {
    return this
      .httpClient
      .get<Measurement>(`${environment.apiUrl}calculation/list/${sortOrder}/${page}/${pageSize}`)
      .pipe(
        take(1),
        map(result => Either.makeRight(result)),
        catchError(getErrorHandler<Measurement>('listMeasurements'))
      )
  }

  constructor(private httpClient: HttpClient) { }
}
