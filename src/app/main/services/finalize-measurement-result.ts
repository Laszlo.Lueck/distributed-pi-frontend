export interface FinalizeMeasurementResult {
  session: string;
  result: string;
  additionalText: string;
  calculationTimeMs: number;
  state: number;
}
