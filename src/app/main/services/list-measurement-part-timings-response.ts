export interface ListMeasurementPartTimingsResponse {
  timings: ListMeasurementPartTimingEntry[];
}

export interface ListMeasurementPartTimingEntry {
  id: number;
  calculationTimeMs: number;
  resultSize: number;
}
