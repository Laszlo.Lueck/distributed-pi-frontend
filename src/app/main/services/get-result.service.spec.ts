import { TestBed } from '@angular/core/testing';

import { GetResultService } from './get-result.service';
import {HttpClient} from "@angular/common/http";

describe('GetResultService', () => {
  let service: GetResultService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    service = new GetResultService(httpClientSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
