import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {catchError, map, take} from "rxjs";
import {Either} from "../../generic/Either";
import {getErrorHandler} from "../../generic/helper";
import {FinalizeMeasurementResult} from "./finalize-measurement-result";
import {FinalizeMeasurementRequest} from "./finalize-measurement-request";

@Injectable({
  providedIn: 'root'
})
export class FinalizeMeasurementService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  public finalizeMeasurement(sessionId: string) {
    const request: FinalizeMeasurementRequest = {
      sessionId: sessionId
    };

    return this
      .httpClient
      .put<FinalizeMeasurementResult>(`${environment.apiUrl}calculation/finalize`, request, this.httpOptions)
      .pipe(
        take(1),
        map(result => Either.makeRight(result)),
        catchError(getErrorHandler<FinalizeMeasurementResult>('finalizeMeasurement'))
      )
  }


  constructor(private httpClient: HttpClient) {
  }
}
