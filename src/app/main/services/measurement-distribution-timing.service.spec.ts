import { TestBed } from '@angular/core/testing';

import { MeasurementDistributionTimingService } from './measurement-distribution-timing.service';
import {HttpClient} from "@angular/common/http";

describe('MeasurementDistributionTimingService', () => {
  let service: MeasurementDistributionTimingService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;


  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    service = new MeasurementDistributionTimingService(httpClientSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
