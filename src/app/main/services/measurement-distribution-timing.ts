export interface MeasurementDistributionTiming {
  iteration: number;
  processTimeMs: number | null | undefined;
}
