import {MeasurementDistributionTiming} from "./measurement-distribution-timing";

export interface MeasurementByDevice {
  processTimes: MeasurementDistributionTiming[];
  hostName: string;
  totalProcessTime: number;
}
