import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {catchError, map, take} from "rxjs";
import {environment} from "../../../environments/environment";
import {Either} from "../../generic/Either";
import {GetResultDto} from "./get-result-dto";
import {getErrorHandler} from "../../generic/helper";
import {DeleteResultDto} from "./delete-result-dto";

@Injectable({
  providedIn: 'root'
})
export class DeleteMeasurementService {


  public deleteMeasurement(sessionId: string) {
    return this
      .httpClient
      .delete<DeleteResultDto>(`${environment.apiUrl}calculation/delete/${sessionId}`)
      .pipe(
        take(1),
        map(result => Either.makeRight(result)),
        catchError(getErrorHandler<DeleteResultDto>('deleteMeasurement'))
      )
  }

  constructor(private httpClient: HttpClient) { }
}
