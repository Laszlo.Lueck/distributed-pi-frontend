import { TestBed } from '@angular/core/testing';

import { CreateMeasurementService } from './create-measurement.service';
import {HttpClient} from "@angular/common/http";

describe('CreateMeasurementService', () => {
  let service: CreateMeasurementService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['put']);
    service = new CreateMeasurementService(httpClientSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
