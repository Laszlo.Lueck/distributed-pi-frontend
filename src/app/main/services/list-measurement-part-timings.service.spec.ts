import { TestBed } from '@angular/core/testing';

import { ListMeasurementPartTimingsService } from './list-measurement-part-timings.service';
import {HttpClient} from "@angular/common/http";

describe('ListMeasurementPartTimingsService', () => {
  let service: ListMeasurementPartTimingsService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    service = new ListMeasurementPartTimingsService(httpClientSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
