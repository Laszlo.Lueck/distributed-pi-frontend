export interface CreateMeasurementRequest {
  iterations: number
  precision: number
}
