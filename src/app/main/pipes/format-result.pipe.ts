import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'formatResult',
    standalone: false
})
export class FormatResultPipe implements PipeTransform {

  transform(value: string, formatString: string, chunkSize: number, decimalSign: string = ","): string {
    const sub = value.startsWith('-') ? 2: 1;
    if(decimalSign.length >= 1) {
      const pre = value.substring(0, sub);
      const rest = value.substring(sub, value.length)

      const inter = this.splitString(rest, chunkSize, formatString);

      return pre + decimalSign + inter;
    } else {
      return this.splitString(value, chunkSize, formatString);
    }
  }


  private splitString(inputString: string, splitLength: number, separator: string): string {
    let parts = [];
    for(let i=0; i<inputString.length; i+=splitLength) {
      parts.push(inputString.slice(i, i + splitLength));
    }
    return parts.join(separator);
  }

}
