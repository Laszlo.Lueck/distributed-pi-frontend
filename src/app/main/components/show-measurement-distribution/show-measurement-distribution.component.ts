import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogActions, MatDialogContent, MatDialogRef} from "@angular/material/dialog";
import {ListMeasurementDistributionTimings} from "../../services/list-measurement-distribution-timings";
import {MatButton} from "@angular/material/button";
import {
  AgChartOptions
} from "ag-charts-community";
@Component({
  selector: 'app-show-measurement-distribution',
  standalone: false,
  templateUrl: './show-measurement-distribution.component.html',
  styleUrl: './show-measurement-distribution.component.css'
})
export class ShowMeasurementDistributionComponent {

  public options: AgChartOptions;

  constructor(
    private matDialogRef: MatDialogRef<ShowMeasurementDistributionComponent>,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) private data: ListMeasurementDistributionTimings
  ) {
    const diagData = data.measurements.map(d => {
      return {node: d.hostName, amount: d.processTimes.length, totalTime: d.totalProcessTime};
    });


    this.options = {
      title: {text: 'Verteilung pro Workernode'},
      subtitle: {text: 'Prozentuale Verteilung der Berechnungen pro Node'},
      data: diagData,
      series: [
        {
          type: "pie",
          angleKey: "amount",
          calloutLabelKey: "node",
          sectorLabelKey: "amount",
          sectorLabel: {
            color: "white",
            fontWeight: "bold",
          },
        },
      ],
    };

  }



  onCloseClick(): void {
    this.matDialogRef.close();
  }
}
