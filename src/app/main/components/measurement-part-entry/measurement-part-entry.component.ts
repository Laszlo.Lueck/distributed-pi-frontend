import {Component, Input} from '@angular/core';
import {ListMeasurementPart} from "../../services/list-measurement-parts-response";

@Component({
    selector: 'app-measurement-part-entry',
    templateUrl: './measurement-part-entry.component.html',
    styleUrl: './measurement-part-entry.component.css',
    standalone: false
})
export class MeasurementPartEntryComponent {
  @Input() measurementPart!: ListMeasurementPart;


}
