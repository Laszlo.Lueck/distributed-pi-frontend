import {Component, EventEmitter, Input, Output} from '@angular/core';
import {MeasurementDto} from "../../services/measurementDto";
import {MatDialog} from "@angular/material/dialog";
import {ResultDetailComponent} from "../result-detail/result-detail.component";
import {ResultDetailRequestDto} from "../result-detail/result-detail-request-dto";
import {ShowMeasurementPartsComponent} from "../show-measurement-parts/show-measurement-parts.component";
import {ShowMeasurementTimingsComponent} from "../show-measurement-timings/show-measurement-timings.component";
import {ListMeasurementPartTimingsService} from "../../services/list-measurement-part-timings.service";
import {Either} from "../../../generic/Either";
import {BaseError} from "../../../generic/baseError";
import {ListMeasurementPartTimingsResponse} from "../../services/list-measurement-part-timings-response";
import {ActionErrorDialogData, DialogType} from "../action-error-dialog/action-error-dialog-data";
import {ActionErrorDialogComponent} from "../action-error-dialog/action-error-dialog.component";
import {MeasurementDistributionTimingService} from "../../services/measurement-distribution-timing.service";
import {ListMeasurementDistributionTimings} from "../../services/list-measurement-distribution-timings";
import {
  ShowMeasurementDistributionComponent
} from "../show-measurement-distribution/show-measurement-distribution.component";
import {ShowMeasurementsPerNodeComponent} from "../show-measurements-per-node/show-measurements-per-node.component";

@Component({
    selector: 'app-main-entry',
    templateUrl: './main-entry.component.html',
    styleUrl: './main-entry.component.css',
    standalone: false
})
export class MainEntryComponent {
  @Input() measurementDto!: MeasurementDto;
  @Output() deleteEntry: EventEmitter<string> = new EventEmitter<string>();
  @Output() finalizeEntry: EventEmitter<string> = new EventEmitter<string>();


  constructor(
    public matDialog: MatDialog,
    private listMeasurementPartTimingsService: ListMeasurementPartTimingsService,
    private measurementDistributionService: MeasurementDistributionTimingService
  ) {
  }

  getIcon(isTrue: boolean): string {
    return isTrue ? 'check' : 'close';
  }

  getClass(isTrue: boolean): string {
    return isTrue ? 'mat-icon-ok' : 'mat-icon-not-ok';
  }

  setActiveButtonColor(isTrue: boolean, trueColor: string, falseColor: string): string {
    return isTrue ? trueColor : falseColor;
  }

  emitDeleteEntry(): void {
    this.deleteEntry.emit(this.measurementDto.id);
  }

  emitFinalizeEntry(): void {
    this.finalizeEntry.emit(this.measurementDto.id);
  }


  openMeasurementPartsDialog(sessionId: string): void {
    this.matDialog.open(ShowMeasurementPartsComponent, {
      enterAnimationDuration: '0.5s',
      exitAnimationDuration: '0.5s',
      width: '90%',
      maxWidth: '90vw',
      height: '70%',
      maxHeight: '70vh',
      panelClass: 'custom',
      data: sessionId
    });
  }

  openDistributionDialog(sessionId: string): void {
    this
      .measurementDistributionService
      .listMeasurementDistributionTimings(sessionId)
      .subscribe((result: Either<BaseError, ListMeasurementDistributionTimings>): void => {
        result.match(left => {
            const dt: ActionErrorDialogData = {
              visibleForSeconds: 5,
              headline: 'Ein Fehler ist aufgetreten',
              body: `Funktion: ${left.operation}`,
              code: JSON.stringify(left.errorMessage, null, 3),
              dialogType: DialogType.Error
            };
            this.matDialog.open(ActionErrorDialogComponent, {
              width: '100%',
              maxWidth: '100vw',
              enterAnimationDuration: '0.5s',
              exitAnimationDuration: '0.5s',
              position: {top: '0px'},
              panelClass: 'custom',
              data: dt
            });
          },
          right => {

            this.matDialog.open(ShowMeasurementDistributionComponent, {
              enterAnimationDuration: '0.5s',
              exitAnimationDuration: '0.5s',
              width: '30%',
              maxWidth: '30vw',
              height: '60%',
              maxHeight: '60vh',
              panelClass: 'custom',
              data: right
            });
          })
      });
  }

  openStatisticsDialog(sessionId: string): void {
    this
      .measurementDistributionService
      .listMeasurementDistributionTimings(sessionId)
      .subscribe((result: Either<BaseError, ListMeasurementDistributionTimings>): void => {
        result.match(left => {
            const dt: ActionErrorDialogData = {
              visibleForSeconds: 5,
              headline: 'Ein Fehler ist aufgetreten',
              body: `Funktion: ${left.operation}`,
              code: JSON.stringify(left.errorMessage, null, 3),
              dialogType: DialogType.Error
            };
            this.matDialog.open(ActionErrorDialogComponent, {
              width: '100%',
              maxWidth: '100vw',
              enterAnimationDuration: '0.5s',
              exitAnimationDuration: '0.5s',
              position: {top: '0px'},
              panelClass: 'custom',
              data: dt
            });
          },
          right => {

            this.matDialog.open(ShowMeasurementsPerNodeComponent, {
              enterAnimationDuration: '0.5s',
              exitAnimationDuration: '0.5s',
              width: '80%',
              maxWidth: '80vw',
              height: '60%',
              maxHeight: '60vh',
              panelClass: 'custom',
              data: right
            });
          })
      });
  }

  openTimingsDialog(sessionId: string): void {
    this
      .listMeasurementPartTimingsService
      .listMeasurementPartTimings(sessionId)
      .subscribe((result: Either<BaseError, ListMeasurementPartTimingsResponse>): void => {
        result.match(left => {
            const dt: ActionErrorDialogData = {
              visibleForSeconds: 5,
              headline: 'Ein Fehler ist aufgetreten',
              body: `Funktion: ${left.operation}`,
              code: JSON.stringify(left.errorMessage, null, 3),
              dialogType: DialogType.Error
            };
            this.matDialog.open(ActionErrorDialogComponent, {
              width: '100%',
              maxWidth: '100vw',
              enterAnimationDuration: '0.5s',
              exitAnimationDuration: '0.5s',
              position: {top: '0px'},
              panelClass: 'custom',
              data: dt
            });
          },
          right => {

            this.matDialog.open(ShowMeasurementTimingsComponent, {
              enterAnimationDuration: '0.5s',
              exitAnimationDuration: '0.5s',
              width: '90%',
              maxWidth: '90vw',
              height: '70%',
              maxHeight: '70vh',
              panelClass: 'custom',
              data: right.timings
            });
          })
      });


  }

  openResultDialog(sessionId: string): void {

    const transport: ResultDetailRequestDto = {
      sessionId: sessionId,
      calculationTime: this.measurementDto.finalizedTimeMs,
      iterations: this.measurementDto.iterations,
      precision: this.measurementDto.precision
    }
    this.matDialog.open(ResultDetailComponent, {
      enterAnimationDuration: '0.5s',
      exitAnimationDuration: '0.5s',
      width: '90%',
      maxWidth: '90vw',
      height: '80%',
      maxHeight: '80vh',
      panelClass: 'custom',
      data: transport
    });

  }

}
