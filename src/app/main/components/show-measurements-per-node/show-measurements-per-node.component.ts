import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogActions, MatDialogContent, MatDialogRef} from "@angular/material/dialog";
import {MatButton} from "@angular/material/button";
import {AgChartOptions, AgCartesianSeriesOptions} from "ag-charts-community";
import {AgCharts} from "ag-charts-angular";
import {ListMeasurementDistributionTimings} from "../../services/list-measurement-distribution-timings";
import {MeasurementDistributionTiming} from "../../services/measurement-distribution-timing";

@Component({
    selector: 'app-show-measurements-per-node',
    imports: [
        MatDialogContent,
        MatDialogActions,
        MatButton,
        AgCharts
    ],
    templateUrl: './show-measurements-per-node.component.html',
    styleUrl: './show-measurements-per-node.component.css'
})
export class ShowMeasurementsPerNodeComponent {
  public options: AgChartOptions;


  fillMissingIterations(data: MeasurementDistributionTiming[], start: number, end: number): MeasurementDistributionTiming[] {
    const filledData: MeasurementDistributionTiming[] = [];
    const dataMap = new Map(data.map(item => [item.iteration, item.processTimeMs]));

    for (let i = start; i <= end; i++) {
      filledData.push({
        iteration: i,
        processTimeMs: dataMap.get(i) !== undefined ? dataMap.get(i) : null
      });
    }

    return filledData;
  }

  constructor(
    private matDialogRef: MatDialogRef<ShowMeasurementsPerNodeComponent>,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) private data: ListMeasurementDistributionTimings
  ) {
    const mx = data
      .measurements
      .map(d => d.processTimes.map(d => d.iteration))
      .reduce((a, v) => a.concat(v))


    const x: AgCartesianSeriesOptions[] = data
      .measurements
      .sort((a, b) => a.hostName.localeCompare(b.hostName))
      .map((d) => ({
        type: 'line',
        strokeWidth: 2,
        xKey: 'iteration',
        yKey: 'processTimeMs',
        xName: 'Iteration',
        yName: d.hostName,
        data: this.fillMissingIterations(d.processTimes, Math.min(...mx), Math.max(...mx)),
        marker: {
          enabled: true,
          size: 4
        },
        connectMissingData: false,
        interpolation: {type: "smooth"}
      }));

    this.options = {
      title: {text: 'Messdauer pro Node'},
      subtitle: {text: ''},
      series: x,
      axes: [
        {
          type: "number",
          position: "bottom",
          label: {
            format: "#(.0f)"
          }
        },
        {
          type: "number",
          position: "left",
          label: {
            format: "#{.2f} ms",
          },
        },
      ]
    }

  }

  onCloseClick(): void {
    this.matDialogRef.close();
  }

}
