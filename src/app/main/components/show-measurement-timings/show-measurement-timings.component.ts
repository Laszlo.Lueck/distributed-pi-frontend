import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {
  AgAxisLabelFormatterParams, AgChartOptions
} from "ag-charts-community";
import {
  ListMeasurementPartTimingEntry
} from "../../services/list-measurement-part-timings-response";

@Component({
    selector: 'app-show-measurement-timings',
    templateUrl: './show-measurement-timings.component.html',
    styleUrl: './show-measurement-timings.component.css',
    standalone: false
})
export class ShowMeasurementTimingsComponent {
  public options: AgChartOptions;

  constructor(
    private matDialogRef: MatDialogRef<ShowMeasurementTimingsComponent>,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) private data: ListMeasurementPartTimingEntry[]
  ) {

    this.options = {
      title: {text: 'Dauer der Einzelmessungen'},
      subtitle: {text: 'Timings der Berechung '},
      data: this.data,
      series: [
        {
          type: "bar",
          xKey: "id",
          yKey: "resultSize",
          yName: "Größe Teilergebnis",
          fill: "lightblue",
          cornerRadius: 2,
        },
        {
          type: "line",
          xKey: "id",
          stroke: "red",
          strokeWidth: 2,
          yKey: "calculationTimeMs",
          yName: "Berechnungszeit (ms)",
          marker: {
            shape: "circle",
            size: 1,
            fill: "red",
            stroke: "red",
          }
        },
      ],
      axes: [
        {
          type: "category",
          position: "bottom",
        },
        {
          type: "number",
          position: "right",
          keys: ["resultSize"],
          label: {
            formatter: (params : AgAxisLabelFormatterParams) => {
              return params.value + " B";
            },
          },
        },
        {
          type: "number",
          position: "left",
          keys: ["calculationTimeMs"],
          label: {
            formatter: (params: AgAxisLabelFormatterParams) => {
              return params.value + " ms";
            },
          },
        },
      ]
    };

  }

  onCloseClick(): void {
    this.matDialogRef.close();
  }

}
