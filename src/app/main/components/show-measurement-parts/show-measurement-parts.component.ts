import {Component, Inject, OnInit} from '@angular/core';
import {ListMeasurementPartsService} from "../../services/list-measurement-parts.service";
import {Either} from "../../../generic/Either";
import {ListMeasurementPart, ListMeasurementPartsResponse} from "../../services/list-measurement-parts-response";
import {BaseError} from "../../../generic/baseError";
import {ActionErrorDialogData, DialogType} from "../action-error-dialog/action-error-dialog-data";
import {ActionErrorDialogComponent} from "../action-error-dialog/action-error-dialog.component";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {PageEvent} from "@angular/material/paginator";

@Component({
    selector: 'app-show-measurement-parts',
    templateUrl: './show-measurement-parts.component.html',
    styleUrl: './show-measurement-parts.component.css',
    standalone: false
})
export class ShowMeasurementPartsComponent implements OnInit {
  length: number = 10;
  pageSize: number = 10;
  pageIndex: number = 0;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  hidePageSize: boolean = false;
  showPageSizeOptions: boolean = true;
  showFirstLastButtons: boolean = true;
  disabled: boolean = false;
  pageEvent!: PageEvent;
  public entries!: ListMeasurementPart[];

  constructor(
    private listMeasurementPartsService: ListMeasurementPartsService,
    private matDialogRef: MatDialogRef<ShowMeasurementPartsComponent>,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) private sessionId: string
  ) {

  }

  handlePageEvent(e: PageEvent) {
    console.log("Event is: ");
    console.log(e);

    this.pageEvent = e;
    this.length = e.length;
    this.pageSize = e.pageSize;
    this.pageIndex = e.pageIndex;
    this.loadData();
  }

  ngOnInit(): void {
    this.loadData()
  }

  loadData(): void {
    this
      .listMeasurementPartsService
      .listMeasurementParts(this.sessionId, this.pageIndex + 1, this.pageSize)
      .subscribe((result: Either<BaseError, ListMeasurementPartsResponse>): void => {
        result.match(
          left => {
            const dt: ActionErrorDialogData = {
              visibleForSeconds: 5,
              headline: 'Ein Fehler ist aufgetreten',
              body: `Funktion: ${left.operation}`,
              code: JSON.stringify(left.errorMessage, null, 3),
              dialogType: DialogType.Error
            };
            this.dialog.open(ActionErrorDialogComponent, {
              width: '100%',
              maxWidth: '100vw',
              enterAnimationDuration: '0.5s',
              exitAnimationDuration: '0.5s',
              position: {top: '0px'},
              panelClass: 'custom',
              data: dt
            });
          },
          right => {
            this.length = right.docCount;
            this.entries = right.measurementPartList;
          })
      });
  }

  onCloseClick(): void {
    this.matDialogRef.close();
  }

}
