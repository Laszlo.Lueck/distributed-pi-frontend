import {Component, OnDestroy, OnInit} from '@angular/core';
import {CommonDataService} from "../../../services/CommonDataService";
import {Subscription} from "rxjs";

@Component({
    selector: 'app-navigation',
    templateUrl: './navigation.component.html',
    styleUrl: './navigation.component.css',
    standalone: false
})
export class NavigationComponent implements OnInit, OnDestroy {
  subscription!: Subscription;
  title!: string;

  constructor(private commonDataService: CommonDataService) {

  }

  ngOnInit(): void {
    this.subscription = this.commonDataService.getData().subscribe(data => {
      this.title = data;
    })
  }

  ngOnDestroy(): void {
    if(this.commonDataService){
      this.commonDataService.clearData();
      this.subscription.unsubscribe();
    }
  }

}
