import {Component, OnDestroy, OnInit} from '@angular/core';
import {CommonDataService} from "../../../services/CommonDataService";
import {Subscription} from "rxjs";
import {ListMeasurementsService} from "../../services/list-measurements.service";
import {Either} from "../../../generic/Either";
import {BaseError} from "../../../generic/baseError";
import {Measurement} from "../../services/measurement";
import {ActionErrorDialogData, DialogType} from "../action-error-dialog/action-error-dialog-data";
import {MatDialog} from "@angular/material/dialog";
import {ActionErrorDialogComponent} from "../action-error-dialog/action-error-dialog.component";
import {PageEvent} from "@angular/material/paginator";
import {MeasurementDto} from "../../services/measurementDto";
import {DeleteMeasurementService} from "../../services/delete-measurement.service";
import {DeleteResultDto} from "../../services/delete-result-dto";
import {CreateMeasurementComponent} from "../create-measurement/create-measurement.component";
import {CreateMeasurementDto} from "../create-measurement/create-measurement-dto";
import {FinalizeMeasurementService} from "../../services/finalize-measurement.service";
import {FinalizeMeasurementResult} from "../../services/finalize-measurement-result";
import {SortOrder} from "../../../services/sort-order";

interface SortOrderSelect  {
  value: SortOrder;
  viewValue: string;
}

@Component({
    selector: 'app-main',
    templateUrl: './main.component.html',
    styleUrl: './main.component.css',
    standalone: false
})
export class MainComponent implements OnInit, OnDestroy {
  length: number = 10;
  pageSize: number = 10;
  pageIndex: number = 0;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  hidePageSize: boolean = false;
  showPageSizeOptions: boolean = true;
  showFirstLastButtons: boolean = true;
  disabled: boolean = false;

  sortOrder: SortOrder = SortOrder.ASC;
  sos: SortOrderSelect[] = [
    {value: SortOrder.ASC, viewValue: 'aufsteigend'},
    {value: SortOrder.DESC, viewValue: 'absteigend'}
  ];

  public entries!: MeasurementDto[];

  pageEvent!: PageEvent;
  private subscription!: Subscription;

  constructor(
    private commonDataService: CommonDataService,
    private listMeasurementService: ListMeasurementsService,
    private deleteMeasurementService: DeleteMeasurementService,
    private finalizeMeasurementService: FinalizeMeasurementService,
    private dialog: MatDialog
  ) {


  }

  ngOnInit(): void {
    this.commonDataService.sendData('Startseite');
    this.loadData()
  }

  ngOnDestroy(): void {
    if (this.subscription)
      this.subscription.unsubscribe();
  }

  openNewMeasurementDialog(): void {
    const createMeasurementDto: CreateMeasurementDto =
      {
        shouldRefresh: false,
        precision: 1,
        iterations: 1
      };

    const dialogRef = this
      .dialog
      .open(CreateMeasurementComponent, {
        enterAnimationDuration: '0.5s',
        exitAnimationDuration: '0.5s',
        width: '40%',
        maxWidth: '40vw',
        height: '40%',
        maxHeight: '40vh',
        panelClass: 'custom',
        data: createMeasurementDto
      });

    dialogRef
      .afterClosed()
      .subscribe(result => {
        if (result && result.shouldRefresh) {
          this.loadData();
        }
      })

  }

  handlePageEvent(e: PageEvent) {
    console.log("Event is: ");
    console.log(e);

    this.pageEvent = e;
    this.length = e.length;
    this.pageSize = e.pageSize;
    this.pageIndex = e.pageIndex;
    this.loadData();
  }

  handleDeleteRequest(eventHandler: string): void {
    this.deleteMeasurement(eventHandler)
  }

  handleFinalizeRequest(eventHandler: string): void {
    this.finalizeMeasurement(eventHandler)
  }

  private getDialogDataFromState(result: FinalizeMeasurementResult): ActionErrorDialogData {
    const dt: ActionErrorDialogData = {
      visibleForSeconds: 5,
      headline: 'Erfolgreich abgeschlossen',
      body: `Funktion: finalizeMeasurement`,
      code: JSON.stringify(result, null, 3),
      dialogType: DialogType.Success
    }

    if (result.state !== 200) {
      dt.dialogType = DialogType.Warning;
      dt.headline = 'Es sind Probleme aufgetreten';
    }

    return dt;

  }

  finalizeMeasurement(sessionId: string): void {
    this
      .finalizeMeasurementService
      .finalizeMeasurement(sessionId)
      .subscribe((result: Either<BaseError, FinalizeMeasurementResult>): void => {
        result.match(left => {
          const dt: ActionErrorDialogData = {
            visibleForSeconds: 5,
            headline: 'Ein Fehler ist aufgetreten',
            body: `Funktion: ${left.operation}`,
            code: JSON.stringify(left.errorMessage, null, 3),
            dialogType: DialogType.Error
          };
          this.dialog.open(ActionErrorDialogComponent, {
            width: '100%',
            maxWidth: '100vw',
            enterAnimationDuration: '0.5s',
            exitAnimationDuration: '0.5s',
            position: {top: '0px'},
            panelClass: 'custom',
            data: dt
          });
        }, right => {
          const data = this.getDialogDataFromState(right);
          this.dialog.open(ActionErrorDialogComponent, {
            width: '100%',
            maxWidth: '100vw',
            enterAnimationDuration: '0.5s',
            exitAnimationDuration: '0.5s',
            position: {top: '0px'},
            panelClass: 'custom',
            data: data
          });

          this.loadData()
        })
      })
  }

  deleteMeasurement(sessionId: string): void {
    this
      .deleteMeasurementService
      .deleteMeasurement(sessionId)
      .subscribe((result: Either<BaseError, DeleteResultDto>): void => {
        result.match(left => {
          const dt: ActionErrorDialogData = {
            visibleForSeconds: 5,
            headline: 'Ein Fehler ist aufgetreten',
            body: `Funktion: ${left.operation}`,
            code: JSON.stringify(left.errorMessage, null, 3),
            dialogType: DialogType.Error
          };
          this.dialog.open(ActionErrorDialogComponent, {
            width: '100%',
            maxWidth: '100vw',
            enterAnimationDuration: '0.5s',
            exitAnimationDuration: '0.5s',
            position: {top: '0px'},
            panelClass: 'custom',
            data: dt
          });
        }, right => {
          const dt: ActionErrorDialogData = {
            visibleForSeconds: 5,
            headline: 'Erfolg',
            body: 'Funktion: DeleteMeasurement',
            code: JSON.stringify(right, null, 3),
            dialogType: DialogType.Success
          };
          this.dialog.open(ActionErrorDialogComponent, {
            width: '100%',
            maxWidth: '100vw',
            enterAnimationDuration: '0.5s',
            exitAnimationDuration: '0.5s',
            position: {top: '0px'},
            panelClass: 'custom',
            data: dt
          });
          this.loadData();
        });
      })
  }

  loadData(): void {
    this.subscription = this
      .listMeasurementService
      .listMeasurements(this.sortOrder, this.pageIndex + 1, this.pageSize)
      .subscribe((data: Either<BaseError, Measurement>): void => {
        data.match(left => {
          const dt: ActionErrorDialogData = {
            visibleForSeconds: 5,
            headline: 'Ein Fehler ist aufgetreten',
            body: `Funktion: ${left.operation}`,
            code: JSON.stringify(left.errorMessage, null, 3),
            dialogType: DialogType.Error
          };
          this.dialog.open(ActionErrorDialogComponent, {
            width: '100%',
            maxWidth: '100vw',
            enterAnimationDuration: '0.5s',
            exitAnimationDuration: '0.5s',
            position: {top: '0px'},
            panelClass: 'custom',
            data: dt
          });
        }, right => {
          this.length = right.docCount;
          this.entries = right.measurementList;
        })
      });
  }

}
