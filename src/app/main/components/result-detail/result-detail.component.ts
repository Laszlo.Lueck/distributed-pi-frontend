import {Component, Inject, OnInit} from '@angular/core';
import {DialogRef} from "@angular/cdk/dialog";
import {ResultDetailRequestDto} from "./result-detail-request-dto";
import {
  MAT_DIALOG_DATA,
  MatDialog,
} from "@angular/material/dialog";
import {GetResultService} from "../../services/get-result.service";
import {BaseError} from "../../../generic/baseError";
import {GetResultDto} from "../../services/get-result-dto";
import {Either} from "../../../generic/Either";
import {ActionErrorDialogData, DialogType} from "../action-error-dialog/action-error-dialog-data";
import {ActionErrorDialogComponent} from "../action-error-dialog/action-error-dialog.component";

@Component({
    selector: 'app-result-detail',
    templateUrl: './result-detail.component.html',
    styleUrl: './result-detail.component.css',
    standalone: false
})
export class ResultDetailComponent implements OnInit {

  public result!: string;

  constructor(
    private matDialogRef: DialogRef<ResultDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ResultDetailRequestDto,
    private getResultService: GetResultService,
    private dialog: MatDialog) {
  }


  ngOnInit(): void {
    this.loadData();
  }

  loadData(): void {
    this
      .getResultService
      .getResultDto(this.data.sessionId)
      .subscribe((result: Either<BaseError, GetResultDto>): void  => {
        result.match(left => {
          const dt: ActionErrorDialogData ={
            visibleForSeconds: 5,
            headline: 'Ein Fehler ist aufgetreten',
            body: `Funktion: ${left.operation}`,
            code: JSON.stringify(left.errorMessage, null, 3),
            dialogType: DialogType.Error
          };
          this.dialog.open(ActionErrorDialogComponent, {
            width: '100%',
            maxWidth: '100vw',
            enterAnimationDuration: '0.5s',
            exitAnimationDuration: '0.5s',
            position: {top: '0px'},
            panelClass: 'custom',
            data: dt
          });
        }, right => {
          this.result = right.result;
        });
      })
  }

  onCloseClick(): void {
    this.matDialogRef.close();
  }

}
