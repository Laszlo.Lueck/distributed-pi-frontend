export interface ResultDetailRequestDto{
  sessionId: string;
  calculationTime: number;
  iterations: number;
  precision: number;
}
