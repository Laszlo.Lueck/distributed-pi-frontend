import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {ActionErrorDialogData, DialogType} from "./action-error-dialog-data";

@Component({
    selector: 'app-action-error-dialog',
    templateUrl: './action-error-dialog.component.html',
    styleUrls: ['./action-error-dialog.component.css'],
    standalone: false
})
export class ActionErrorDialogComponent {
  public progressValue: number;

  setHeaderStyle(): string {
    switch (this.data.dialogType) {
      case DialogType.Success:
        return "background-color: rgba(86, 153, 86, 0.6);";
      case DialogType.Error:
        return "background-color: rgba(196, 0, 0, 0.55);";
      case DialogType.Warning:
        return "background-color: rgba(255, 165, 0, 0.9);";
      default:
        return "background-color: rgba(200, 200, 200, 0.8);";
    }
  }

  constructor(public dialogRef: MatDialogRef<ActionErrorDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: ActionErrorDialogData) {
    this.progressValue = 0;

    dialogRef
      .afterOpened()
      .subscribe(_ => {
        setInterval(() => {
          this.progressValue += 1;
        }, ((data.visibleForSeconds * 1000) / 105));

        setTimeout(() => {
          dialogRef.close();
        }, data.visibleForSeconds * 1000);
      });

  }


}
