import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {CreateMeasurementService} from "../../services/create-measurement.service";
import {Either} from "../../../generic/Either";
import {BaseError} from "../../../generic/baseError";
import {CreateMeasurementResponse} from "../../services/create-measurement-response";
import {ActionErrorDialogData, DialogType} from "../action-error-dialog/action-error-dialog-data";
import {ActionErrorDialogComponent} from "../action-error-dialog/action-error-dialog.component";
import {CreateMeasurementDto} from "./create-measurement-dto";
import {NgModel} from "@angular/forms";

@Component({
    selector: 'app-create-measurement',
    templateUrl: './create-measurement.component.html',
    styleUrl: './create-measurement.component.css',
    standalone: false
})
export class CreateMeasurementComponent {
  constructor(
    private matDialogRef: MatDialogRef<CreateMeasurementComponent>,
    private createMeasurementService: CreateMeasurementService,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: CreateMeasurementDto
  ) {
  }

  errorMessage(fieldName: string): string {
    return `Das Feld ${fieldName} ist ein Pflichtfeld`;
  }


  checkValidation(model: NgModel): boolean {
    return (model && model.invalid)??false;
  }

  checkElements(elements: NgModel[]): boolean {
    return elements.filter(element => element.invalid).length > 0;
  }

  onCreateClick(): void {
    this
      .createMeasurementService
      .createMeasurement(this.data.iterations, this.data.precision)
      .subscribe((result: Either<BaseError, CreateMeasurementResponse>): void => {
        result.match(left => {
          const dt: ActionErrorDialogData = {
            visibleForSeconds: 5,
            headline: 'Ein Fehler ist aufgetreten',
            body: `Funktion: ${left.operation}`,
            code: JSON.stringify(left.errorMessage, null, 3),
            dialogType: DialogType.Error
          };
          this.dialog.open(ActionErrorDialogComponent, {
            width: '100%',
            maxWidth: '100vw',
            enterAnimationDuration: '0.5s',
            exitAnimationDuration: '0.5s',
            position: {top: '0px'},
            panelClass: 'custom',
            data: dt
          });
        }, right => {
          console.log(JSON.stringify(right, null, 3));
          if(right.state === 200){
            const dt: ActionErrorDialogData = {
              visibleForSeconds: 5,
              headline: 'Erfolg',
              body: 'Funktion: CreateMeasurement',
              code: JSON.stringify(right, null, 3),
              dialogType: DialogType.Success
            };
            this.dialog.open(ActionErrorDialogComponent, {
              width: '100%',
              maxWidth: '100vw',
              enterAnimationDuration: '0.5s',
              exitAnimationDuration: '0.5s',
              position: {top: '0px'},
              panelClass: 'custom',
              data: dt
            });
            this.data.shouldRefresh = true;
            this.matDialogRef.close(this.data);
          }
          if(right.state === 400){
            const dt: ActionErrorDialogData = {
              visibleForSeconds: 5,
              headline: 'Fehler',
              body: 'Funktion: CreateMeasurement',
              code: JSON.stringify(right, null, 3),
              dialogType: DialogType.Warning
            };
            this.dialog.open(ActionErrorDialogComponent, {
              width: '100%',
              maxWidth: '100vw',
              enterAnimationDuration: '0.5s',
              exitAnimationDuration: '0.5s',
              position: {top: '0px'},
              panelClass: 'custom',
              data: dt
            });
          }
        });
      });
  }

  onCloseClick(): void {
    this.data.shouldRefresh = false;
    this.matDialogRef.close(this.data);
  }

}
