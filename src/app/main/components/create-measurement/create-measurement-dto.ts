export interface CreateMeasurementDto {
  shouldRefresh: boolean;
  precision: number;
  iterations: number;
}
