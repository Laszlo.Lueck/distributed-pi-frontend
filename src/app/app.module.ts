import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import {CommonDataService} from "./services/CommonDataService";
import {NavigationComponent} from "./main/components/navigation/navigation.component";
import {MatIcon} from "@angular/material/icon";
import {MatToolbar} from "@angular/material/toolbar";
import {MatDivider} from "@angular/material/divider";
import {MatButton} from "@angular/material/button";
import {RouterLink} from "@angular/router";
import {provideHttpClient} from "@angular/common/http";
import {MainComponent} from "./main/components/main/main.component";
import {MatCard, MatCardContent} from "@angular/material/card";
import {ActionErrorDialogComponent} from "./main/components/action-error-dialog/action-error-dialog.component";
import {MatProgressBar} from "@angular/material/progress-bar";
import {MatDialogActions, MatDialogClose, MatDialogContent, MatDialogTitle} from "@angular/material/dialog";
import {MatPaginator, MatPaginatorIntl} from "@angular/material/paginator";
import {MainEntryComponent} from "./main/components/main-entry/main-entry.component";
import {PaginatorLocalization} from "./main/components/main/PaginatorLocalization";
import {MatFormField, MatLabel} from "@angular/material/form-field";
import {MatGridList, MatGridTile} from "@angular/material/grid-list";
import {ResultDetailComponent} from "./main/components/result-detail/result-detail.component";
import {FormatResultPipe} from "./main/pipes/format-result.pipe";
import {CreateMeasurementComponent} from "./main/components/create-measurement/create-measurement.component";
import {MatInput} from "@angular/material/input";
import {FormsModule} from "@angular/forms";
import {MatOption, MatSelect} from "@angular/material/select";
import {ShowMeasurementPartsComponent} from "./main/components/show-measurement-parts/show-measurement-parts.component";
import {MeasurementPartEntryComponent} from "./main/components/measurement-part-entry/measurement-part-entry.component";
import {
  ShowMeasurementTimingsComponent
} from "./main/components/show-measurement-timings/show-measurement-timings.component";
import {
  ShowMeasurementDistributionComponent
} from "./main/components/show-measurement-distribution/show-measurement-distribution.component";
import {AgCharts, AgChartsModule} from "ag-charts-angular";


@NgModule({
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [
    AppComponent,
    NavigationComponent,
    MainComponent,
    ActionErrorDialogComponent,
    ResultDetailComponent,
    FormatResultPipe,
    MainEntryComponent,
    CreateMeasurementComponent,
    ShowMeasurementPartsComponent,
    MeasurementPartEntryComponent,
    ShowMeasurementTimingsComponent,
    ShowMeasurementDistributionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatIcon,
    MatToolbar,
    MatDivider,
    MatButton,
    RouterLink,
    MatCard,
    MatCardContent,
    MatProgressBar,
    MatDialogContent,
    MatDialogTitle,
    MatPaginator,
    MatLabel,
    MatGridList,
    MatGridTile,
    MatDialogActions,
    MatFormField,
    MatInput,
    FormsModule,
    MatDialogClose,
    MatSelect,
    MatOption,
    AgCharts, AgChartsModule
  ],
  providers: [
    provideAnimationsAsync(),
    provideHttpClient(),
    CommonDataService,
    {provide: MatPaginatorIntl, useValue: PaginatorLocalization()}
  ],
  exports: [
    FormatResultPipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
