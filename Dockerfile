FROM harbor.gretzki.ddns.net/docker-cache/nginx:1.27.2-bookworm

COPY external/nginx.conf /etc/nginx/conf.d/default.conf
COPY output/ /usr/share/nginx/html/

EXPOSE 80
